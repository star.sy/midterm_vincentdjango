from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import WidgetUser
from .forms import UserForm


def index(request):
        users = WidgetUser.objects.all()
        return render(request, 'dashboard/dashboard.html', {'users': users, })

class UserDetailView(DetailView):
        model = WidgetUser
        template_name = 'dashboard/widgetuser-details.html'

class UserCreateView(CreateView):
        model = WidgetUser
        form_class = UserForm
        template_name = 'dashboard/widgetuser-add.html'

class UserUpdateView(UpdateView):
        model = WidgetUser
        form_class = UserForm
        template_name = 'dashboard/widgetuser-edit.html'
