from django.urls import path
from .views import index, UserDetailView, UserCreateView, UserUpdateView


urlpatterns = [
    path('dashboard/', index, name='index'),
    path('widgetusers/<int:pk>/details/', UserDetailView.as_view(), name='user-details'),
    path('widgetusers/add/', UserCreateView.as_view(), name='user-add'),
    path('widgetusers/<int:pk>/edit/', UserUpdateView.as_view(), name='user-edit'),
]


app_name = "dashboard"