from django.forms.models import BaseModelForm
from django.http import HttpResponse
from django.shortcuts import render
from django.views.generic.detail import DetailView
from django.views.generic.list import ListView
from django.views.generic.edit import CreateView, UpdateView
from django.urls import reverse, reverse_lazy

from .models import Assignment


def homeAssignmentsPage(request):
    return render(request, 'assignments/assignments.html', {"assignmentsList":Assignment.objects.all()}) #you can pass an entire list of model instances to the html


class AssignmentsDetailView(DetailView):
    model = Assignment
    template_name = "assignments/assignment-details.html"


class AssignmentsUpdateView(UpdateView):
    model = Assignment
    fields = ["assignment_name","description","course","perfect_score" ]
    template_name = "assignments/assignment-edit.html"

    success_url = "../details"


class AssignmentsCreateView(CreateView):
    model = Assignment
    fields = ["assignment_name","description","course","perfect_score" ]
    template_name = "assignments/assignment-add.html"

    def get_success_url(self):
        return reverse('assignments:assignment_details', kwargs={ #appName:pathName
            'pk': self.object.pk,
        })
