from django.contrib import admin
from .models import ForumPost, Reply

class ForumPostAdmin(admin.ModelAdmin):
    model = ForumPost

    list_display = ('title', 'body', 'author', 'pub_datetime')
    search_fields = ('title', 'author', 'pub_datetime',)
    list_filter = ('title', 'author', 'pub_datetime',)

class ReplyAdmin(admin.ModelAdmin):
    model = Reply
    
    list_display = ('body', 'post', 'author', 'pub_datetime',)
    search_fields = ('author', 'pub_datetime',)
    list_filter = ('author', 'pub_datetime',)

admin.site.register(ForumPost, ForumPostAdmin)
admin.site.register(Reply, ReplyAdmin)
