from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from dashboard.models import WidgetUser
from .models import ForumPost, Reply


def index(request):
    forumposts = ForumPost.objects.all()
    replies = Reply.objects.all()
    return render(request, 'forum/forum.html', {'forumposts': forumposts, 'replies': replies})

class ForumPostDetailView(DetailView):
    model = ForumPost
    template_name = 'forum/forumpost-details.html'

class ForumPostCreateView(CreateView):
    model = ForumPost
    template_name = 'forum/forumpost-add.html'
    fields = ["title", "body", "author"]

class ForumPostUpdateView(UpdateView):
    model = ForumPost
    template_name = 'forum/forumpost-edit.html'
    fields = '__all__'
