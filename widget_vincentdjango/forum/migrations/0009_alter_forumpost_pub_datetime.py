# Generated by Django 3.2 on 2023-05-12 06:46

import datetime
from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('forum', '0008_alter_forumpost_pub_datetime'),
    ]

    operations = [
        migrations.AlterField(
            model_name='forumpost',
            name='pub_datetime',
            field=models.DateTimeField(default=datetime.datetime(2023, 5, 12, 14, 46, 44, 130987), editable=False),
        ),
    ]
