from django.shortcuts import render
from django.http import HttpResponse
from django.views.generic.detail import DetailView
from django.views.generic.edit import CreateView, UpdateView

from .models import Event, Location


def index(request):
    events = Event.objects.all()
    return render(request, 'calendar/calendar.html',
                  {'events': events, })


class EventDetailView(DetailView):
    model = Event
    template_name = 'calendar/event-details.html'


class EventCreateView(CreateView):
    model = Event
    template_name = 'calendar/event-add.html'
    fields = ["activity", "target_datetime", "estimated_hours", "location", "course"]


class EventUpdateView(UpdateView):
    model = Event
    template_name = 'calendar/event-edit.html'
    fields = '__all__'
