from django.db import models
from django.urls import reverse
from datetime import datetime
from assignments.models import Course


class Location(models.Model):
    mode = models.CharField(max_length=100, default="")
    venue = models.CharField(max_length=100, default="")

    def __str__(self):
        return '{} {}'.format(self.mode, self.venue)


class Event(models.Model):
    target_datetime = models.DateTimeField(default=datetime.now())
    activity = models.CharField(max_length=100, default="")
    estimated_hours = models.FloatField(default=0)
    course = models.ForeignKey(
        Course,
        on_delete=models.CASCADE,
    )
    location = models.ForeignKey(
        Location,
        on_delete=models.CASCADE,
        related_name='venues'
    )

    def __str__(self):
        return "{} {} {} {}".format(self.target_datetime, self.activity, self.estimated_hours, self.location)

    def get_absolute_url(self):
        return reverse('calendar:event-details', kwargs={'pk': self.pk})
