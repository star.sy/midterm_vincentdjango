from django.contrib import admin
from .models import Event, Location


class EventInline(admin.TabularInline):
    model = Event


class EventAdmin(admin.ModelAdmin):
    model = Event

    list_display = ('activity', 'target_datetime', 'estimated_hours', 'course', 'location')
    search_fields = ('activity', 'target_datetime', 'estimated_hours', 'course', 'location')
    list_filter = ('activity', 'location')

    fieldsets = [
        ('Event', {
            'fields':
                (('target_datetime', 'activity'), 'estimated_hours', 'course', 'location',),
        }),
    ]


class LocationAdmin(admin.ModelAdmin):
    model = Location

    list_display = ('mode', 'venue')
    search_fields = ('mode', 'venue')
    list_filter = ('mode', 'venue')

    fieldsets = [
        ('Mode & Venue', {
            'fields':
                ('mode', 'venue')
        })

    ]


admin.site.register(Event, EventAdmin)
admin.site.register(Location, LocationAdmin)
