from django.contrib import admin
from .models import Announcement, Reaction

class ReactionInline(admin.TabularInline):
    model = Reaction

class AnnouncementAdmin(admin.ModelAdmin):
    model = Announcement

    list_display = ('title', 'author', )
    search_fields = ('title', 'body', 'author', )
    list_filter = ('title', 'author')

    fieldsets = [
        ('Announcement', {
            'fields':
                ('title', 'body','author')
        })
    ]

    inlines = [ReactionInline]

class ReactionAdmin(admin.ModelAdmin):
    model = Reaction

    list_display = ('name', 'tally', 'announcement')
    search_fields = ('name', 'announcement')
    list_filter = ('name', 'announcement')


admin.site.register(Announcement, AnnouncementAdmin)
admin.site.register(Reaction, ReactionAdmin)
