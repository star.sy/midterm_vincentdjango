from django.urls import path
from .views import index, AnnouncementsDetailView, AnnouncementsCreateView, AnnouncementsUpdateView

urlpatterns = [
    path('', index, name='index'),
    path('announcements/<int:pk>/details', AnnouncementsDetailView.as_view(), name='announcements-detail'),
    path('announcements/add', AnnouncementsCreateView.as_view(), name = 'announcements-add'),
    path('announcements/<int:pk>/edit', AnnouncementsUpdateView.as_view(), name = 'announcements-edit')
]


app_name = "announcements"